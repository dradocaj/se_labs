# Domagoj Radocaj #
Mali Soricak 5 
32237 Lovas
dradocaj@etfos.hr

Rođen 26.06.1995 u Zagrebu.

## Obrazovanje ##
- Završena osnovna škola Lovas
- Završena Gimnazija Vukovar
- Trenutno 3. godina stručnog studija Automatika na Fakultetu elektrotehnike, računarstva i informacijskih tehnologija Osijek 

Bez radnog iskustva 

## Jezici ##
- Hrvatski jezik (materinji)
- Engleski jezik (fluentan u čitanju i pisanju)
