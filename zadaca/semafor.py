# ukljucivanje biblioteke pygame
import pygame


 
pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 512
HEIGHT = 512

crvena= pygame.image.load("crveno.png")
zuta= pygame.image.load("zuto.png")
zelena= pygame.image.load("zelena.png")
crvenozuto= pygame.image.load("crvenozuto.png")
crna= pygame.image.load("crna.png")
crvena= pygame.transform.scale(crvena,(WIDTH,HEIGHT))
zuta= pygame.transform.scale(zuta,(WIDTH,HEIGHT))
zelena = pygame.transform.scale(zelena,(WIDTH,HEIGHT))
crvenozuto = pygame.transform.scale(crvenozuto,(WIDTH,HEIGHT))
crna = pygame.transform.scale(crna,(WIDTH,HEIGHT))

# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Semafor")
 
clock = pygame.time.Clock()
i = 0
duration = 25000

done = False
while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
                 
    while duration >=18000:
        screen.blit(crvena,(0,0))
        pygame.display.flip()
    while 17999>duration>=15000:
        screen.blit(crvenozuto,(0,0))
        pygame.display.flip()
    while 14999>duration>=7000:
        screen.blit(zelena(0,0))
        pygame.display.flip()
    while 6999>duration>=6500:
        screen.blit(crna,(0,0))
        pygame.display.flip()
    while 6499>duration>=6000:
        screen.blit(zelena(0,0))
        pygame.display.flip()
    while 5999>duration>=5500:
        screen.blit(crna,(0,0))
        pygame.display.flip()
    while 5499>duration>=4000:
        screen.blit(zelena(0,0))
        pygame.display.flip()
    while 3999>duration>=3500:
        screen.blit(crna,(0,0))
        pygame.display.flip()
    while 3499>duration>=3000:
        screen.blit(zelena(0,0))
        pygame.display.flip()
    while 2999>duration:
        screen.blit(zuta,(0,0))
        pygame.display.flip()
    if duration<=0:
        duration=25000



    pygame.display.flip()

    
    
   
    #ukoliko je potrebno ceka do iscrtavanja
    #iduceg framea kako bi imao 60fpsa
    time=clock.tick(60)
    duration= duration-time
    print(duration)
pygame.quit()
