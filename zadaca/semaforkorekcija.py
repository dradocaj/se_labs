# ukljucivanje biblioteke pygame
import pygame


 
pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 512
HEIGHT = 512

crvena= pygame.image.load("crveno.png")
zuta= pygame.image.load("zuto.png")
zelena= pygame.image.load("zelena.png")
crvenozuto= pygame.image.load("crvenozuto.png")
crna= pygame.image.load("crna.png")
crvena= pygame.transform.scale(crvena,(WIDTH,HEIGHT))
zuta= pygame.transform.scale(zuta,(WIDTH,HEIGHT))
zelena = pygame.transform.scale(zelena,(WIDTH,HEIGHT))
crvenozuto = pygame.transform.scale(crvenozuto,(WIDTH,HEIGHT))
crna = pygame.transform.scale(crna,(WIDTH,HEIGHT))

# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Semafor")
 
clock = pygame.time.Clock()
i = 0
duration = 25000

done = False
while not done:      
    if duration >=18000:
        screen.blit(crvena,(0,0))
        pygame.display.flip()
    elif 17999>duration>=15000:
        screen.blit(crvenozuto,(0,0))
        pygame.display.flip()
    elif 14999>duration>=7000:
        screen.blit(zelena,(0,0))
        pygame.display.flip()
    elif 6999>duration>=6500:
        screen.blit(crna,(0,0))
        pygame.display.flip()
    elif 6499>duration>=6000:
        screen.blit(zelena,(0,0))
        pygame.display.flip()
    elif 5999>duration>=5500:
        screen.blit(crna,(0,0))
        pygame.display.flip()
    elif 5499>duration>=5000:
        screen.blit(zelena,(0,0))
        pygame.display.flip()
    elif 4999>duration>=4500:
        screen.blit(crna,(0,0))
        pygame.display.flip()
    elif 4499>duration>=4000:
        screen.blit(zelena,(0,0))
        pygame.display.flip()
    elif 3999>duration>1:
        screen.blit(zuta,(0,0))
        pygame.display.flip()
    else:
        duration=25000



    pygame.display.flip()

    
    
   
    #ukoliko je potrebno ceka do iscrtavanja
    #iduceg framea kako bi imao 60fpsa
    time=clock.tick(60)
    duration= duration-time
pygame.quit()