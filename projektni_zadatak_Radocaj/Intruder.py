import pygame
import random
 
pygame.init()
pygame.font.init()
intruder = ""
final=[]
def make(niz1,niz2,niz3,niz4):
  if niz1[0]==1:
    final=random.sample(niz2,4)
    if niz1[1]==2:
      intruder=random.choice(niz3)
      final.append(intruder)
    else:
      intruder=random.choice(niz4)
      final.append(intruder)
  elif niz1[0]==2:
    final=random.sample(niz3,4)
    if niz1[1]==1:
      intruder=random.choice(niz2)
      final.append(intruder)
    else:
      intruder=random.choice(niz4)
      final.append(intruder)
  elif niz1[0]==3:
    final=random.sample(niz4,4)
    if niz1[1]==1:
      intruder=random.choice(niz2)
      final.append(intruder)
    else:
      intruder=random.choice(niz3)
      final.append(intruder)
  final=random.shuffle(final)
    

  

WIDTH = 1600
HEIGHT = 900

RED = [ 255, 0, 0 ]
YELLOW = [255,255,0]

size = (WIDTH, HEIGHT)

#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Find the intruder")

#load images and scale them properly
# cca 15 pics + 2 background
startscreen=pygame.image.load("background1.jpg")
endscreen=pygame.image.load("background2.jpeg")
startscreen= pygame.transform.scale(startscreen,(WIDTH,HEIGHT))
endscreen= pygame.transform.scale(endscreen,(WIDTH,HEIGHT))

a1=pygame.image.load("cat.png")
a2=pygame.image.load("dog.png")
a3=pygame.image.load("elephant.png")
a4=pygame.image.load("Bird.png")
a5=pygame.image.load("pig.png")

a1=pygame.transform.scale(a1, (100,100))
a2=pygame.transform.scale(a2, (100,100))
a3=pygame.transform.scale(a3, (100,100))
a4=pygame.transform.scale(a4, (100,100))
a5=pygame.transform.scale(a5, (100,100))
animals=[a1,a2,a3,a4,a5] #pics with animals

f1=pygame.image.load("carrot.png")
f2=pygame.image.load("hamburger.png")
f3=pygame.image.load("hotdog.png")
f4=pygame.image.load("pizza.png")
f5=pygame.image.load("soup.png")

f1=pygame.transform.scale(f1, (100,100))
f2=pygame.transform.scale(f2, (100,100))
f3=pygame.transform.scale(f3, (100,100))
f4=pygame.transform.scale(f4, (100,100))
f5=pygame.transform.scale(f5, (100,100))

food=[f1,f2,f3,f4,f5] #pics with food
i1=pygame.image.load("bed.png")
i2=pygame.image.load("car.png")
i3=pygame.image.load("laptop.png")
i4=pygame.image.load("plane.png")
i5=pygame.image.load("table.png")

i1=pygame.transform.scale(i1, (100,100))
i2=pygame.transform.scale(i2, (100,100))
i3=pygame.transform.scale(i3, (100,100))
i4=pygame.transform.scale(i4, (100,100))
i5=pygame.transform.scale(i5, (100,100))
items=[i1,i2,i3,i4,i5] #pics with everyday items


nizovi=random.sample([1,2,3],2)

#font and text for states
myfont= pygame.font.SysFont('Arial',40)
start_text= myfont.render("Press SPACE to play!",False, RED)
end_text=myfont.render("Press SPACE to play again!",False, RED)
scorefont= pygame.font.SysFont('Arial', 72)


state="start" # play and endgame
clock = pygame.time.Clock()

iconx1=200
iconx2=300
iconx3=400
iconx4=500
iconx5=600
final=[]
final=make(nizovi,animals,food,items)
counter=0
score = 0
done = False
iconpos1= (iconx1,HEIGHT/2)
iconpos2= (iconx2,HEIGHT/2)
iconpos3= (iconx3,HEIGHT/2)
iconpos4= (iconx4,HEIGHT/2)
iconpos5= (iconx5,HEIGHT/2)
while not done:

  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      done = True
    if event.type == pygame.KEYDOWN:
      if event.key == pygame.K_SPACE:
          if state =="start":
            state="endgame"
          elif state=="endgame":
            state="play"
            score=0
    if event.type==pygame.MOUSEBUTTONDOWN:
      if iconposx1.collidepoint(event.pos):
          helpvar=0
          if final[helpvar]==intruder:
            score = score + 1
            counter = counter +1
            nizovi=random.sample([1,2,3],2)
            final=make(nizovi,animals,food,items)
          else:
            counter = counter +1
            nizovi=random.sample([1,2,3],2)
            final=make(nizovi,animals,food,items)
          if counter == 10:
            state= "endgame"
      elif iconposx2.collidepoint(event.pos):
          helpvar=1
          if final[helpvar]==intruder:
            score = score + 1
            counter = counter +1
            nizovi=random.sample([1,2,3],2)
            final=shuffle(nizovi,animals,food,items)
          else:
            counter = counter +1
            nizovi=random.sample([1,2,3],2)
            final=make(nizovi,animals,food,items)
          if counter == 10:
            state= "endgame"
      elif iconposx3.collidepoint(event.pos):
          helpvar=2
          if final[helpvar]==intruder:
            score = score + 1
            counter = counter +1
            nizovi=random.sample([1,2,3],2)
            final=make(nizovi,animals,food,items)
          else:
            counter = counter +1
            nizovi=random.sample([1,2,3],2)
            final=make(nizovi,animals,food,items)
          if counter == 10:
            state= "endgame"
      elif iconposx4.collidepoint(event.pos):
          helpvar=3
          if final[helpvar]==intruder:
            score = score + 1
            counter = counter +1
            nizovi=random.sample([1,2,3],2)
            final=make(nizovi,animals,food,items)
          else:
            counter = counter +1
            nizovi=random.sample([1,2,3],2)
            final=make(nizovi,animals,food,items)
          if counter == 10:
            state= "endgame"
      elif iconposx5.collidepoint(event.pos):
          helpvar=4
          if final[helpvar]==intruder:
            score = score + 1
            counter = counter +1
            nizovi=random.sample([1,2,3],2)
            final=make(nizovi,animals,food,items)
          else:
            counter = counter +1
            nizovi=random.sample([1,2,3],2)
            final=make(nizovi,animals,food,items)
          if counter == 10:
            state= "endgame"

    #screen
    if state== "start":
        screen.blit(startscreen,(0,0))
        screen.blit(start_text,(30,30))
    

    elif state=="play":
        screen.fill(YELLOW)
        screen.blit(final[0],iconpos1)
        screen.blit(final[1],iconpos2)
        screen.blit(final[2],iconpos3)
        screen.blit(final[3],iconpos4)
        screen.blit(final[4],iconpos5)
    

    elif state=="endgame":
        screen.fill(YELLOW)
        screen.blit(endscreen,(0,0))
        score_text= scorefont.render("Score: %d /10" %score, False, RED)
        screen.blit(score_text,(0,10))
        screen.blit(end_text,(600,30))
    

    pygame.display.flip()
   

time = clock.tick(60)
pygame.quit()