# ukljucivanje biblioteke pygame
import pygame

def next_bg(color):
    if color== TIRKIZ:
        color= RED
    elif color==RED:
        color= MAGENTA
    elif color == MAGENTA:
        color=BLUE
    elif color==BLUE:
        color = TIRKIZ
    return color
def next_bgdva(image):
    if image==bg_img:
        image=bg_img2
    elif image==bg_img2:
        image=bg_img3
    elif image==bg_img3:
        image=bg_img
    return image
 
pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1600
HEIGHT = 900

RED = [255,0,0]
GREEN = [0,255,0]
BLUE = [0,0,255]
TIRKIZ= [64,224,208]
MAGENTA= [255,0,255]

bg_img= pygame.image.load("bg.jpg")
bg_img= pygame.transform.scale(bg_img,(WIDTH,HEIGHT))
bg_img2= pygame.image.load("bg2.jpg")
bg_img3= pygame.image.load("bg3.jpg")
bg_img2= pygame.transform.scale(bg_img2,(WIDTH,HEIGHT))
bg_img3= pygame.transform.scale(bg_img3,(WIDTH,HEIGHT))

# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")
 
clock = pygame.time.Clock()
i = 0
duration = 1000
bg_color=TIRKIZ
bg=bg_img
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key==pygame.K_SPACE:
                bg_color=next_bg(bg_color)
                
    i +=1
    if duration <0:
        duration =1000
        bg=next_bgdva(bg)
    
    screen.fill(bg_color)
    screen.blit(bg,(0,0))
    pygame.display.flip()
   
    #ukoliko je potrebno ceka do iscrtavanja
    #iduceg framea kako bi imao 60fpsa
    time=clock.tick(60)
    duration= duration-time
    #print(duration)
pygame.quit()